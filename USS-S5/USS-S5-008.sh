#!/bin/bash

echo "Program to sort array"

echo "Enter length of an array"
read n

i=0
echo "Enter eleents "
while [ $i -lt $n ]
do
	read arr[$i]
	i=`expr $i + 1`
done
i=0
while [ $i -lt $n ]
do
	j=$i
	while [ $j -lt $n ]
	do
		if [ ${arr[$j]} -lt ${arr[$i]} ]
		then
			temp=${arr[$i]}
			arr[$i]=${arr[$j]}
			arr[$j]=$temp	
		fi
		j=`expr $j + 1`
	done
	i=`expr $i + 1`
done

i=0
echo "Array after sorting is "
while [ $i -lt $n ]
do
	echo -n "${arr[$i]} "
	i=`expr $i + 1`
done
echo " "
