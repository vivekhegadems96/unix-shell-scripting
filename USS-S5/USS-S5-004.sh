#!/bin/bash

echo "Program using for loop"

a=0
for i in {1..10}
do
  sum=`expr $sum + $i`
done
echo "Sum of 10 natural numbers = $sum"
