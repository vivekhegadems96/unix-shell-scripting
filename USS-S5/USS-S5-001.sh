#!/bin/bash

echo "Program to illustrate multiple if statements"
echo "Enter two numbers"
read a
read b

if [ $a -gt $b ]
then
	echo "$a is greater"
elif [ $b -gt $a ]
then
	echo "$b is greater"
else
	echo "both are equal"
fi
