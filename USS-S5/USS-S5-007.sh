#!/bin/bash

echo "Program to illustrate switch statement"

echo "Enter two variables"
read a
read b

echo "Enter +,*,- or /"
read cha
case $cha in "+") echo `expr $a + $b`;;
	"-") echo `expr $a - $b`;;
	"*") echo `expr $a * $b`;;
	"/") echo `expr $a / $b`;;
	*) echo "Invalid entry";;
esac
