#!/bin/bash

echo "Program to illustrate while loop"
echo "Enter a number"
read a

while [ $a -lt 5 ]
do
	echo 'in loop'
	a=`expr $a + 1`
done
echo "Exited the while loop"
echo "a = $a"
