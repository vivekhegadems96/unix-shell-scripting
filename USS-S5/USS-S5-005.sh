#!/bin/bash

echo "Program using break and continue statements"

a=0
while [ $a -ne 10 ]
do
	a=`expr $a + 1`
	if [ $a -eq 5 ]
	then
		echo "Loop broke"
		break
	fi
	continue
done
echo "Value = $a"
