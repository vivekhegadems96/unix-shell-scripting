#!/bin/bash

echo "Test Operators"

EXPRESSION=true
STRING_WITH_VALUE="Hello World"
STRING_WITHOUT_VALUE=""
STRING1_EQUAL="Wipro"
STRING2_EQUAL="Wipro"
STRING1_NOT_EQUAL="Hello"
STRING2_NOT_EQUAL="Hi"
INTEGER1=5
INTEGER2=5
INTEGER3=100
INTEGER4=20
FILE="Test"
DIRECTORY="New"

echo "EXPRESSION value is: $EXPRESSION"
echo "Inverting value of EXPRESSION..."
!$EXPRESSION
echo "Now value of EXPRESSION is: $EXPRESSION"

if [ -n $STRING_WITH_VALUE ]
then
  echo "The length of string is greater than zero!"
  echo "String used for testing is $STRING_WITH_VALUE"
fi

if [ -z $STRING_WITHOUT_VALUE ]
then
  echo "The srting length is zero!"
  echo "String used for testing is $STRING_WITHOUT_VALUE"
fi


if [ $STRING1_EQUAL = $STRING2_EQUAL ]
then
  echo "Both strings are equal"
  echo "Value of first string is: $STRING1_EQUAL"
  echo "Value of first string is: $STRING2_EQUAL"
fi

if [ $STRING1_NOT_EQUAL != $STRING2_NOT_EQUAL ]
then
  echo "Both strings are equal"
  echo "Value of first string is: $STRING1_NOT_EQUAL"
  echo "Value of first string is: $STRING2_NOT_EQUAL"
fi

if [ $INTEGER1 -eq $INTEGER2 ]
then
  echo "Numbers are equal"
  echo "First number is: $INTEGER1"
  echo "Second number is: $INTEGER2"
fi

if [ $INTEGER3 -gt $INTEGER4 ]
then
  echo "Number $INTEGER3 is greater than $INTEGER4"
fi

if [ $INTEGER4 -lt $INTEGER3 ]
then
  echo "Number $INTEGER4 is less than $INTEGER3"
fi

if [ -d $DIRECTORY ]
then
  echo "Directory with name $DIRECTORY exists"
else
  echo "Directory with name $DIRECTORY not found"
fi

if [ -e $FILE ]
then
  echo "$FILE file exists"
else
  echo "$FILE file not found"
fi

if [ -s $FILE ]
then
  echo "$FILE file exists and it's size is greater than zero"
fi

if [ -r $FILE ]
then
  echo "$FILE file exists and read permission granted"
else
  echo "$FILE file exists and read permission is not granted"
fi

if [ -w $FILE ]
then
  echo "$FILE file exists and write permission is granted"
else
  echo "$FILE file exists and write permission is not granted"
fi

if [ -x $FILE ]
then
  echo "$FILE file exists and execute permission is granted"
else
  echo "$FILE file exists and execute permission is not granted"
fi
