#!/bin/bash

echo "Setting Execution Permissions Script"

echo "Setting execution path"
set PATH=$PATH:$PWD/unix_assignments/
echo "Done"

echo "Setting execution mode"
chmod 777 ./USS-S1-002.sh
echo "Done"
