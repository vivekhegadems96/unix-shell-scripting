#!/bin/bash

echo "Multiple Piping"

declare -a filenames
filenames=$(ls -s ./ | grep "Test*" | grep 0 | awk '{ print $2 }')
for file in $filenames
do
  echo "Deleting file: $file"
  rm -rf $file
done
