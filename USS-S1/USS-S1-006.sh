#!/bin/bash

echo "Redirection to File With/Without appending"

echo "Executing system command: ps"
ps > output.txt
echo "Done"

echo "Executing system command: ls -l"
ls -l >> output.txt
echo "Done"
