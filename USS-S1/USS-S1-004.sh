#!/bin/bash

echo "Redirection to STDOUT"

echo "Listing the process and redirecting to a file"
ps > process.txt

echo "Printing data from redirected file"
cat process.txt
