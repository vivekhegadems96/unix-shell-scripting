#!/bin/bash

echo "Regular Expressions"

echo "************ Start ************"
whatever="hello"
echo "Regular expresessions and descriptions:"
echo "1. At least one a followed by any number of b's: a+b*"
echo "2. Any number of backslashes followed by any number of asterisks: \\*\**"
echo "3. Three consecutive copies of whatever is contained in $whatever: ($whatever){3}"
echo "4. Any five characters, including newline: (.|\n){5}"
echo "5. The same word written two or more times in a row (with possibly varying intervening whitespace), where \"word\" is defined as a nonempty sequence of nonwhitespace characters: (^|\s)(\S+)(\s+\2)+(\s|$)"
echo "************ Done ************"

echo "************ Start ************"
bash myprogram.sh /usr/dict/words "normal"
bash myprogram.sh /usr/dict/words "modified"
echo "************ Done ************"


echo "************ Start ************"
IFS=':'
while read -r user password uid gid description home shell; do
  echo "Username is '$user' and complete info of user is '$description'"
done </etc/passwd
echo "************ Done ************"
