#!/bin/bash

string=$2
normal_string="normal"
modified_string="modified"

if [ $string = $normal_string ]
then
  echo "Command output that contains all vowels"
  cat $1 | awk '/a/ && /e/ && /i/ && /o/ && /u/ { print }'
elif [ $string = $modified_string ]
then
  echo "Modified output of command to get words with vowels in sequence"
  cat $1 | grep -E ".*a.*e.*i.*o.*u.*"
else
  echo "Failed!"
fi
