#!/bin/bash

echo "Program to pass a parameter to the function"
func ()
{
	echo "Parameter passed is $1"
}

echo "Enter some value"
read a
func $a
