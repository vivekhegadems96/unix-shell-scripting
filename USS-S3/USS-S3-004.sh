#!/bin/bash

echo "Command Line Arguments Illustration"
echo "Name of the script is: $0"

if [ $# -ne 0 ]
then
  for arg in "$@"
  do
    echo "Displaying argument from command line: $arg"
  done
else
  echo "No arguments passed"
fi
