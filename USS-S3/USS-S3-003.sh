#!/bin/bash


echo "Shift Command"

if [ $# -ne 0 ]
then
  echo "The arguments provided are:"
  echo "$*"
  shift
  echo "After shifting once:"
  echo $*
  shift
  echo "After shifting twice:"
  echo $*
  shift
  echo "After shifting three times:"
  echo $*
  shift
  echo "After shifting four times:"
  echo $*
else
  echo "No arguments provided"
  echo "Please try again!"
fi
