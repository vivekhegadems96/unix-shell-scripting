#!/bin/bash

echo "Program to illustrate the function with return type"

func()
{
	a=2
	return $a
}

func
ret=$?
echo "Returned value is $ret"
