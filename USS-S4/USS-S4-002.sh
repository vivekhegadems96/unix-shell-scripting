#!/bin/bash

echo "Defining variables and assigning values"
STRING="Hello World"
NUMBER=10
echo "User defined variables are:"
echo "STRING variable with value: $STRING"
echo "NUMBER variable with value: $NUMBER"

echo "-----------------------------------------------------"

echo "Command output to Variable Assignment"
OUTPUT=$(ls -l)
echo "${OUTPUT}"

echo "-----------------------------------------------------"

echo "Assigning variables from Arithmatic functions - Let, Expr and Double Paranthesis $(())"

echo "Usage of let"
let a="3 * (2 + 1)"
echo "Value of a=$a"

echo "Usage of Expr"
count=0
echo "Value of variable count initially is: $count"
count=`expr $count + 1`
echo "Valus of incrementing count by 1 using expr is: $count"

echo "Usage of expansion operator"
echo "$(( 3 * ( 2 + 1 ) ))"
