#!/bin/bash

echo -e "Name of bash script = $0\n"

if [ $# -ne 0 ]
then
  echo -e "All the arguments supplied to the bash script = $*\n"
  echo "Printing each argument seperately"
  for arg in "$@"
  do
    echo "Arguments is: $arg"
  done
else
  echo "No arguments passed"
fi

echo -e "Number of arguments passed = $#\n"
echo -e "Exit status of the most recent run process = $?\n"
echo -e "The process id = $$\n"
echo -e "Username = $USER\n"
echo -e "Hostname of the machine = $HOSTNAME\n"
echo -e "Number of seconds since the script was started = $SECONDS\n"
echo -e "Random number = $RANDOM\n"
echo -e "Current line number = $LINENO\n"
echo -e "Pathname = $PATH\n"
