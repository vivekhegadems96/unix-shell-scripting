#!/bin/bash

echo "Enter the directory name: "
read directory

if [ -d $directory ]
then
  echo "Entered directory with name $directory exists"
  declare -A filenames
  filenames=$(ls -lR $directory | awk '{ print $9 }' | grep -v "^$")
  for file in $filenames
  do
    if [ -f $file ]
    then
      head -1 $file
    else
      echo "$file is a directory"
    fi
  done
else
  echo "Entered directory with name $directory not exists"
  echo "Please enter the proper directory name!"
fi
