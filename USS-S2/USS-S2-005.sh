#!/bin/bash

echo "Disk Usage"

echo "Enter the directory name: "
read directory

if [ -d $directory ]
then
  echo "Entered directory with name $directory exists"
  echo "Printing disk usage of directory: $directory"
  du -ah $directory | grep -v "/$" | sort -rh | head -10
else
  echo "Entered directory with name $directory not exists"
  echo "Please enter the proper directory name!"
fi
