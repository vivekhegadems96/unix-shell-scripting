#!/bin/bash

echo "Enter the directory name: "
read directory

if [ -d $directory ]
then
  echo "Entered directory with name $directory exists"
  echo "Printing all directories, sub-directories and files recursively on size in "
  ls -lSrhR $directory
  echo
  echo "*******************************************************************"
  echo
  echo "Printing all directories, sub-directories and files not recursively based on modification date"
  ls -ltR $directory
else
  echo "Entered directory with name $directory not exists"
  echo "Please enter the proper directory name!"
fi
